from simpy.core import Environment, EmptySchedule, NORMAL, heappop
from simpy.resources.container import Container
import math


class InsufficientCapacity(Exception):
    pass


class CustomEnvironment(Environment):
    def step(self):
        """Process the next event.

        Raise an :exc:`EmptySchedule` if no further events are available.

        """
        try:
            self._now, _, _, event = heappop(self._queue)
        except IndexError:
            raise EmptySchedule()

        # Process callbacks of the event. Set the events callbacks to None
        # immediately to prevent concurrent modifications.
        callbacks, event.callbacks = event.callbacks, []  # <-- when None the next iteration it will crash
        for callback in callbacks:
            callback(event)

        if not event._ok and not hasattr(event, '_defused'):
            # The event has failed and has not been defused. Crash the
            # environment.
            # Create a copy of the failure exception with a new traceback.
            exc = type(event._value)(*event._value.args)
            exc.__cause__ = event._value
            raise exc

    def reschedule(self, event, priority=None, delay=0):
        """Reschedule an *event* with a given *priority* to a new *delay*."""
        for queue_tuple in self._queue:
            if event == queue_tuple[3]:
                self.schedule(event, priority or queue_tuple[1], delay)
                self._queue.remove(queue_tuple)
                break


class CustomContainer(Container):
    def __init__(self, env, linear_constant, capacity_func, init=0):
        self.env = env
        self.last = env.now
        self._linear_constant = linear_constant
        self.capacity_func = capacity_func
        super(CustomContainer, self).__init__(env, capacity_func(), init)

    def _do_get(self, event):
        if self.level >= event.amount:
            self.level -= event.amount
            event.succeed()
            return True

        if event.amount > self.capacity:
            raise InsufficientCapacity(
                'Requested %i resources but container has a maximum capacity '
                'of %i' % (event.amount, self.capacity)
            )

        if not hasattr(event, 'waiting'):
            delay = math.ceil(self.calculate_x(event.amount))
            event.waiting = True
            event.callbacks = [self._trigger_get]
            self.env.schedule(event, delay=delay)
            return False
        else:
            print "moet ik hier wat mee?"

    def calculate_x(self, y):
        # Calculate x time units till y amount of resources
        result = (y - self._level) / self._linear_constant
        assert result >= 0
        return result

    def calculate_y(self, x):
        # Calculate the y amount of resources after x time units
        result = self._linear_constant * x + self._level

        # If result higher than capacity return capacity
        if result > self.capacity:
            result = self.capacity

        return result

    # def immediate_put
    # do not queue put and wait

    @property
    def capacity(self):
        return self.capacity_func()

    @property
    def level(self):
        delta = self.env.now - self.last
        if delta > 0:
            return self.calculate_y(delta)
        return self._level

    @level.setter
    def level(self, value):
        assert value >= 0
        self._level = value
        self.last = self.env.now

    @property
    def linear_constant(self):
        return self._linear_constant

    @linear_constant.setter
    def linear_constant(self, value):
        assert type(value) == float

        # Calculate the current level before applying the new constant
        self.level = self.level

        self._linear_constant = value

        # Make sure all queued events are recalculated
        for event in self.get_queue:
            self.env.reschedule(event, delay=self.calculate_x(event.amount))
