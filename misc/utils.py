import datetime


def date(now):
    return datetime.datetime.fromtimestamp(now).strftime('%H:%M:%S (%d)')