import logging
import os


logger = None


class SocketIOHandler(logging.Handler):

    def __init__(self, socketio=None):
        logging.Handler.__init__(self)
        self.socketio = socketio
        self.level = logging.DEBUG

    def emit(self, record):

        try:
            msg = self.format(record)
            self.socketio.emit('log', msg)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


class CustomAdapter(logging.LoggerAdapter):
    def process(self, msg, kwargs):
        return '[%s] %s' % (self.extra['env'].now, msg), kwargs


def set_adapter(env):
    global logger
    _logger = get_logger()
    logger = CustomAdapter(_logger, {'env': env})
    return logger


def get_logger():
    logger = logging.getLogger('app')
    logger.setLevel(logging.DEBUG)
    verbose_format = logging.Formatter('%(threadName)s - %(asctime)s - %(levelname)s - %(message)s')
    clean_format = logging.Formatter('%(message)s')

    if os.path.isfile('tribalengine.log'):
        fh = logging.FileHandler('tribalengine.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(verbose_format)
        logger.addHandler(fh)

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(clean_format)
    logger.addHandler(ch)
    return logger


if not logger:
    logger = get_logger()

# import logging
# from logging.config import dictConfig
#
# logging_config = {
#     'version': 1,
#     'formatters': {
#         'f': {'format':
#               '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'}
#         },
#     'handlers': {
#         'h': {'class': 'logging.StreamHandler',
#               'formatter': 'f',
#               'level': logging.DEBUG}
#         },
#     'root': {
#         'handlers': ['h'],
#         'level': logging.DEBUG,
#         },
# }
#
# dictConfig(logging_config)
