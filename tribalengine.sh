#! /bin/bash
# Copyright (c) Jurrian Tromp.
# All rights reserved.
#
# Author: Jurrian Tromp, 2014
#
# Please send feedback to jurriantromp@live.nl
#
# /etc/init.d/tribalengine.sh
#
### BEGIN INIT INFO
# Provides: tribalengine
# Required-Start:
# Should-Start:
# Required-Stop:
# Should-Stop:
# Default-Start:  3 5
# Default-Stop:   0 1 2 6
# Short-Description: Tribalengine daemon process
# Description: Tribalengine bot daemon that operates on the Tribalwars API
### END INIT INFO

# Activate the python virtual environment
    . /home/jurrian/tribalengine-env/bin/activate

case "$1" in
  start)
    echo "Starting server"
    # Start the daemon
    python /home/jurrian/tribalengine/tribalengine.py start
    ;;
  stop)
    echo "Stopping server"
    # Stop the daemon
    python /home/jurrian/tribalengine/tribalengine.py stop
    ;;
  restart)
    echo "Restarting server"
    python /home/jurrian/tribalengine/tribalengine.py restart
    ;;
  status)
    echo "Server status:"
    python /home/jurrian/tribalengine/tribalengine.py status
    ;;
  *)
    # Refuse to do other stuff
    echo "Usage: /etc/init.d/tribalengine.sh {start|stop|restart}"
    exit 1
    ;;
esac

exit 0