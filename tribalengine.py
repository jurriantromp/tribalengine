#!/home/jurrian/tribalengine-env/bin/python

from daemon import runner
from main import TribalEngine
import logging
from misc.logger import logger

controller = TribalEngine()
daemon_runner = runner.DaemonRunner(controller)

#logger = logging.getLogger('app')
logger.setLevel(logging.DEBUG)
verbose_format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh = logging.FileHandler('tribalengine.log')
fh.setLevel(logging.INFO)
fh.setFormatter(verbose_format)
logger.addHandler(fh)

# This ensures that the logger file handle does not get closed during daemonization
daemon_runner.daemon_context.files_preserve = [fh.stream]
daemon_runner.do_action()