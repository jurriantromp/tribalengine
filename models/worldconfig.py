from lxml import etree
from lxml import objectify
from urllib2 import urlopen, URLError

from config import ACCOUNTS


class WorldConfig(object):
    def __init__(self, world):
        self.server = "%s.%s" % (world, ACCOUNTS[world]["DOMAIN"])

        try:
            config_data = open("./configs/%s_config.xml" % world).read()
        except OSError:
            try:
                config_data = urlopen("http://%s/interface.php?func=get_config" % self.server).read()
            except URLError:
                raise Exception("Could not load world config data")

        parser = etree.XMLParser(encoding='utf-8')
        self._world_config = etree.fromstring(config_data, parser)

        #assert self._world_config.game.tech == 2
        #__unit_info = urlopen("http://%s/interface.php?func=get_unit_info" % __server).read()

    def get(self, path):
        find = objectify.ObjectPath(path)
        return find(self._world_config)

    def get_float(self, path):
        return float(self.get(path).text)