from lxml import etree
import random
import math


class ReportBase(object):
    def __init__(self):
        self.points = random.randint(50, 560) # todo TEMP!!
        self.date = None

    @property
    def wood_loot(self):
        if not hasattr(self, '_wood_loot'):
            self._wood_loot = random.randint(0, 10) * self.points
        return self._wood_loot

    @property
    def stone_loot(self):
        if not hasattr(self, '_stone_loot'):
            self._stone_loot = random.randint(0, 10) * self.points
        return self._stone_loot

    @property
    def iron_loot(self):
        if not hasattr(self, '_iron_loot'):
            self._iron_loot = random.randint(0, 10) * self.points
        return self._iron_loot

    def loot(self):
        #todo make less in time
        return self.wood_loot + self.stone_loot + self.iron_loot

    def loss(self):
        ran = random.randint(0, 80)
        if ran < 20:
            return math.floor(ran * self.points / 1000)
        return 0