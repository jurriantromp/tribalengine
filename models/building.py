import math
import simpy

from misc.logger import logger


class Buildings(object):
    
    def __init__(self, context, village):
        self.context = context

        self.main = Main(context)
        self.barracks = Barracks(context)
        self.stable = Stable(context)
        self.garage = Garage(context)
        self.church = Church(context)
        self.church_f = ChurchF(context)
        self.snob = Academy(context)
        self.smith = Smith(context)
        self.place = Place(context)
        self.statue = Statue(context)
        self.market = Market(context)
        self.wood = Wood(context, village)
        self.stone = Stone(context, village)
        self.iron = Iron(context, village)
        self.farm = Farm(context)
        self.storage = Storage(context)
        self.hide = Hide(context)
        self.wall = Wall(context)

        for building in context.world_config.get('config.buildings'):
            level = building.xpath("number(.)")
            if level != -1:
                name = building.xpath("name(.)").split('_')[1]
                getattr(self, name).level = int(level)

    def get_buildings(self):
        return [self.main, self.barracks, self.stable, self.garage, self.snob, self.smith, self.church, self.church_f,
                self.place, self.statue, self.market, self.wood, self.stone, self.iron, self.farm, self.storage,
                self.hide, self.wall]


class Building(object):
    build_time_factor = 1.2

    def __init__(self, context):
        self.context = context

        self._level = 0
        self.level = 0

    @property
    def wood_cost(self):
        return math.floor(math.pow(self.wood_factor, self.level - 1) * self.wood)

    @property
    def stone_cost(self):
        return math.floor(math.pow(self.stone_factor, self.level - 1) * self.stone)

    @property
    def iron_cost(self):
        return math.floor(math.pow(self.iron_factor, self.level - 1) * self.iron)

    def upgrade(self):
        self.level += 1

    def set_level(self, level):
        self.level = level

    def cost(self):
        return self.wood_cost + self.stone_cost + self.iron_cost

    def actual_build_time(self, main_level, is_next=False):
        if self.level > 2:
            level = self.level
            if is_next:  # needs to be one more when calculating next level
                level = self.level + 1
            # duration of creation, for lvl 3+	[build_time]*1.18*[build_time_factor]^(level - 1 - 14/(level-1))
            doc = math.pow(self.build_time_factor, level - 1 - (14.0 / (level - 1))) \
                * self.context.world_config.get_float('config.game.buildtime_formula') \
                * (self.build_time / self.context.world_config.get_float('config.speed'))
        else:
            # duration of creation, for lvl 1 and 2	[build_time]*1.18*[build_time_factor]^(-13)
            # zie revisie 42
            doc = math.pow(self.build_time_factor, -13.0) * 1.18 * self.build_time
            # actual build time = [duration of creation]*1.05^(-[level of the village headquarters])
        return math.ceil(math.pow(1.05, -main_level) * doc)

    def upgrade_requirements(self):
        if self.level >= self.max_level:
            return False

        return True  # else: passed tests

    def actual_population(self, next_lvl=False):
        level = self.level + 1 if next_lvl else self.level
        if level < 1:
            return 0
        return math.pow(self.pop_factor, level - 1) * self.pop

    def actual_points(self):
        #points of a building = [points of the building at level 1]*1.2^([level of the building] - 1), rounded to the nearest integer
        return math.floor(math.pow(1.2, self.level - 1) * self.points)


class Resource(Building):
    def __init__(self, context, village):
        super(Resource, self).__init__(context)
        self.village = village
        self._production = None

    def production(self, next_lvl=False):
        if not self._production:
            level = self.level + 1 if next_lvl else self.level
            if level > 0:
                self._production = math.floor(math.pow(1.163118, level - 1.0) * 30 * self.context.world_config.get_float('config.speed')) / 3600  # per second
                # [wood]*[wood_factor]^([level of the building] - 1)
            else:
                self._production = 5 * self.context.world_config.get_float('config.speed') / 3600
        return self._production

    def upgrade(self):
        self._production = None
        getattr(self.village, str(self)).linear_constant = self.production()
        self.level += 1


class Main(Building):
    def __init__(self, context):
        super(Main, self).__init__(context)
        self._level = 1
        self.level = 1
        self.max_level = 30
        self.min_level = 1
        self.wood = 90
        self.stone = 80
        self.iron = 70
        self.pop = 5
        self.wood_factor = 1.26
        self.stone_factor = 1.275
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 900
        self.points = 10
        self.queue = simpy.PriorityResource(self.context.env, capacity=1)

    def __str__(self):
        return "main"


class Barracks(Building):
    def __init__(self, context):
        super(Barracks, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 25
        self.min_level = 0
        self.wood = 200
        self.stone = 170
        self.iron = 90
        self.pop = 7
        self.wood_factor = 1.26
        self.stone_factor = 1.28
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 1800
        self.deps = {"main": 3}
        self.points = 16
        self.queue = simpy.PriorityResource(self.context.env, capacity=1)

    def __str__(self):
        return "barracks"


class Stable(Building):
    def __init__(self, context):
        super(Stable, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 20
        self.min_level = 0
        self.wood = 270
        self.stone = 240
        self.iron = 260
        self.pop = 8
        self.wood_factor = 1.26
        self.stone_factor = 1.28
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 6000
        self.deps = {"main": 10, "barracks": 5, "smith": 5}
        self.points = 20
        self.queue = simpy.PriorityResource(self.context.env, capacity=1)

    def __str__(self):
        return "stable"


class Garage(Building):
    def __init__(self, context):
        super(Garage, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 15
        self.min_level = 0
        self.wood = 300
        self.stone = 240
        self.iron = 260
        self.pop = 8
        self.wood_factor = 1.26
        self.stone_factor = 1.28
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 6000
        self.deps = {"main": 10, "smith": 10}
        self.points = 24

    def __str__(self):
        return "garage"


class Church(Building):
    # not on each world
    def __init__(self, context):
        super(Church, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 3
        self.min_level = 0
        self.wood = 16000
        self.stone = 20000
        self.iron = 5000
        self.pop = 5000
        self.wood_factor = 1.26
        self.stone_factor = 1.28
        self.iron_factor = 1.26
        self.pop_factor = 1.55
        self.build_time = 184980
        self.deps = {"main": 5, "farm": 5}
        self.points = 10

    def __str__(self):
        return "church"


class ChurchF(Building):
    # not on each world
    def __init__(self, context):
        super(ChurchF, self).__init__(context)
        self._level = 1
        self.level = 1
        self.max_level = 1
        self.min_level = 0
        self.wood = 160
        self.stone = 200
        self.iron = 50
        self.pop = 5
        self.wood_factor = 1.26
        self.stone_factor = 1.28
        self.iron_factor = 1.26
        self.pop_factor = 1.55
        self.build_time = 8160
        self.points = 10

    def __str__(self):
        return "church_f"


class Academy(Building):
    def __init__(self, context):
        super(Academy, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 3  # this can be 1 on some worlds
        self.min_level = 0
        self.wood = 15000
        self.stone = 25000
        self.iron = 10000
        self.pop = 80
        self.wood_factor = 2
        self.stone_factor = 2
        self.iron_factor = 2
        self.pop_factor = 1.17
        self.build_time = 586800
        self.deps = {"main": 20, "smith": 20, "market": 10}
        self.points = 512
        self.queue = simpy.PriorityResource(self.context.env, capacity=1)

    def __str__(self):
        return "snob"


class Smith(Building):
    def __init__(self, context):
        super(Smith, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 20
        self.min_level = 0
        self.wood = 220
        self.stone = 180
        self.iron = 240
        self.pop = 20
        self.wood_factor = 1.26
        self.stone_factor = 1.275
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 6000
        self.deps = {"main": 5, "barracks": 1}
        self.points = 19
        self.queue = simpy.PriorityResource(self.context.env, capacity=1)

    def production(self):
        return math.pow(1.1, -self.level)

    def __str__(self):
        return "smith"


class Place(Building):
    def __init__(self, context):
        super(Place, self).__init__(context)
        self._level = 0
        self.level = 1
        self.max_level = 1
        self.min_level = 0
        self.wood = 10
        self.stone = 40
        self.iron = 30
        self.pop = 0
        self.wood_factor = 1.26
        self.stone_factor = 1.275
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 10860
        self.points = 0

    def __str__(self):
        return "place"


class Statue(Building):
    # not on each world
    def __init__(self, context):
        super(Statue, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 1
        self.min_level = 0
        self.wood = 220
        self.stone = 220
        self.iron = 220
        self.pop = 10
        self.wood_factor = 1.26
        self.stone_factor = 1.275
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 1500
        self.points = 24

    def __str__(self):
        return "statue"


class Market(Building):
    def __init__(self, context):
        super(Market, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 25
        self.min_level = 0
        self.wood = 100
        self.stone = 100
        self.iron = 100
        self.pop = 20
        self.wood_factor = 1.26
        self.stone_factor = 1.275
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 2700
        self.deps = {"main": 3, "storage": 2}
        self.points = 10

    def __str__(self):
        return "market"


class Wood(Resource):
    max_level = 30
    min_level = 0
    wood = 50
    stone = 60
    iron = 40
    pop = 5
    wood_factor = 1.25
    stone_factor = 1.275
    iron_factor = 1.245
    pop_factor = 1.155
    build_time = 900
    points = 6

    def __str__(self):
        return "wood"


class Stone(Resource):
    max_level = 30
    min_level = 0
    wood = 65
    stone = 50
    iron = 40
    pop = 10
    wood_factor = 1.27
    stone_factor = 1.265
    iron_factor = 1.24
    pop_factor = 1.14
    build_time = 900
    points = 6

    def __str__(self):
        return "stone"


class Iron(Resource):
    max_level = 30
    min_level = 0
    wood = 75
    stone = 65
    iron = 70
    pop = 10
    wood_factor = 1.252
    stone_factor = 1.275
    iron_factor = 1.24
    pop_factor = 1.17
    build_time = 1080
    points = 6

    def __str__(self):
        return "iron"


class Farm(Building):
    def __init__(self, context):
        super(Farm, self).__init__(context)
        self._capacity = None
        self._level = 1
        self.level = 1
        self.max_level = 30
        self.min_level = 1
        self.wood = 45
        self.stone = 40
        self.iron = 30
        self.pop = 0
        self.wood_factor = 1.3
        self.stone_factor = 1.32
        self.iron_factor = 1.29
        self.pop_factor = 1
        self.build_time = 1200
        self.points = 5

        self.population = simpy.Container(self.context.env, self.capacity())

    def set_level(self, level):
        self.level = level
        self.population = simpy.Container(self.context.env, self.capacity())

    def capacity(self):
        if not self._capacity:
            # 240*1.172103^([level of the farm] - 1), rounded to the nearest integer
            self._capacity = round((math.pow(1.172103, self.level - 1) * 240))
        return self._capacity

    def upgrade(self):
        self.level += 1
        self._capacity = None
        logger.info("Farm capacity is now: %i", self.capacity())

    def __str__(self):
        return "farm"


class Storage(Building):
    def __init__(self, context):
        super(Storage, self).__init__(context)
        self._capacity = None
        self._level = 1
        self.level = 1
        self.max_level = 30
        self.min_level = 1
        self.wood = 60
        self.stone = 50
        self.iron = 40
        self.pop = 0
        self.wood_factor = 1.265
        self.stone_factor = 1.27
        self.iron_factor = 1.15
        self.pop_factor = 1.15
        self.build_time = 1020
        self.points = 6
        self.queue = simpy.PriorityResource(self.context.env, capacity=1)

    def set_level(self, level):
        self.level = level

    def capacity(self):
        # Capacity = 1000*1.2294934^([level of the warehouse] - 1), rounded to the nearest integer
        if not self._capacity:
            self._capacity = math.floor(math.pow(1.2294934, self.level - 1) * 1000)
        return self._capacity

    def upgrade(self):
        self.level += 1
        self._capacity = None
        logger.info("Storage capacity is now: %i", self.capacity())

    def __str__(self):
        return "storage"


class Hide(Building):
    def __init__(self, context):
        super(Hide, self).__init__(context)
        self._level = 1
        self.level = 1
        self.max_level = 10
        self.min_level = 0
        self.wood = 50
        self.stone = 60
        self.iron = 50
        self.pop = 2
        self.wood_factor = 1.25
        self.stone_factor = 1.25
        self.iron_factor = 1.25
        self.pop_factor = 1.17
        self.build_time = 1800
        self.points = 5

    def __str__(self):
        return "hide"


class Wall(Building):
    def __init__(self, context):
        super(Wall, self).__init__(context)
        self._level = 0
        self.level = 0
        self.max_level = 20
        self.min_level = 0
        self.wood = 50
        self.stone = 100
        self.iron = 50
        self.pop = 5
        self.wood_factor = 1.26
        self.stone_factor = 1.275
        self.iron_factor = 1.26
        self.pop_factor = 1.17
        self.build_time = 3600
        self.deps = {"barracks": 1}
        self.points = 8

    def defense(self):
        #factor for defending troops = 1.037^[level of the wall]
        return math.pow(1.037, self.level)

    def __str__(self):
        return "wall"
