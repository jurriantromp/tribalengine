import variables
from config import ACCOUNTS
from interface.session import SessionManager
from models.mapobjects import BotVillage
from misc.logger import logger


class Player(object):
    def __init__(self, context, id=None, name=None):
        self.context = context

        # init basics
        self.id = id
        self.name = name
        self.has_villages = True
        self._villages = dict()
        #self.session = SessionManager(name)
        #self.session.login()

        # call functions
        #self.get_bot_villages()
        #self.execute()

    def __str__(self):
        return self.name or str(id(self))[-3:]

    def execute(self):
        for village in self.all_villages():
            village.execute()

    def set_village(self, x, y, village):
        try:
            self._villages[x][y] = village
        except KeyError:
            self._villages[x] = {y: village}

    def remove_village(self, x, y):
        del self._villages[x][y]
        if not self._villages[x]:
            del self._villages[x]

        if len(self.all_villages()) < 1:
            self.has_villages = False

    def all_villages(self):
        villages = list()
        for x in self._villages.values():
            for y in x.values():
                villages.append(y)
        return villages


class BotPlayer(Player):
    pass


class RealPlayer(Player):
    pass
