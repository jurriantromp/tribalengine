import math
import requests
import json

import variables
from config import ACCOUNTS
from models.mapobjects import PlayerVillage, ActorVillage, BotVillage, Barbarian, Bonus, Cave, Scenery
from models.player import RealPlayer, BotPlayer


class WorldMap(object):

    def __new__(cls, context, map_objects, players, width, height):
        ins = super(WorldMap, cls).__new__(cls)
        ins.context = context
        ins.map_objects = map_objects
        ins.players = players
        ins.width = width
        ins.height = height
        return ins

    @classmethod
    def from_matrix(cls, context, matrix):
        map_objects = []
        players = []
        for y, row in enumerate(matrix, start=0):
            for x, col in enumerate(row, start=0):
                if col == '  ':
                    continue
                elif col == 'Rp':
                    rp = RealPlayer(context)
                    map_object = PlayerVillage(context, x, y, rp)
                    players.append(rp)
                elif col == '^ ':
                    map_object = Scenery(context, x, y)
                elif col == 'Ba':
                    map_object = Barbarian(context, x, y)
                elif col == 'Bo':
                    map_object = Bonus(context, x, y)
                elif col == 'ca':
                    map_object = Cave(context, x, y)
                elif col == 'BP':
                    bp = BotPlayer(context)
                    map_object = BotVillage(context, x, y, bp)
                    players.append(bp)
                else:
                    raise Exception("Invalid matrix")

                map_objects.append(map_object)
        return cls.__new__(cls, context, map_objects, players, len(matrix[0]), len(matrix))

    def get_bot_villages(self):
        player_villages = []
        for map_object in self.map_objects:
            if isinstance(map_object, BotVillage):
                player_villages.append(map_object)
        return player_villages

    def get_actor_villages(self):
        player_villages = []
        for map_object in self.map_objects:
            if isinstance(map_object, ActorVillage):
                player_villages.append(map_object)
        return player_villages

    def from_online(self, bot):  # todo calculate distances for all the villages in the world
        x = int(math.floor((bot.x - 1) / 20) * 20)
        y = int(math.floor((bot.y - 1) / 20) * 20)
        # todo broken (ACCOUNTS)
        map_url = "http://%s.%s/map.php?v=2&%s_%s=1&%s_%s=1&%s_%s=1&%s_%s=1" % (
        variables.local.world, ACCOUNTS[variables.local.world]["DOMAIN"], x, y, x + 20, y, x, y + 20, x + 20, y + 20)
        response = requests.get(map_url)
        views = json.loads(response.text)

        villages = dict()
        for view in views:
            if not view["data"]["villages"]: continue
            data = view["data"]["villages"]
            if isinstance(data, list):
                data = {i: k for i, k in enumerate(data)}
            for xi, row in data.items():
                if isinstance(row, list):
                    row = {i: k for i, k in enumerate(row)}

                for yi, c in row.items():
                    if len(c) == 10:
                        c = c[0]

                    #if not int(c[4]) in players:
                    #    players[int(c[4])] = Player(int(c[4]))

                    if c[2] == 'Bonusdorp':
                        village = Bonus()
                        village.points = int(c[3].replace(".", ""))
                    elif c[3] == '':
                        village = Cave()
                        continue
                    elif c[4] == '0':
                        village = Barbarian()
                        village.points = int(c[3].replace(".", ""))
                    else:
                        village = PlayerVillage()
                        village.name = c[2]
                        village.player = int(c[4])  # players[int(c[4])]
                        village.points = int(c[3].replace(".", ""))
                    village.id = int(c[0])
                    village.x = view["x"] + int(xi)
                    village.y = view["y"] + int(yi)

                    if village.x == bot.x and village.y == bot.y:
                        village.type = 0

                    villages[village.id] = village

        return villages