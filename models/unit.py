import math
import simpy

import variables


class Units(object):
    def __init__(self, context):
        self.context = context

        self.spear = Spear(context)
        self.sword = Sword(context)
        self.axe = Axe(context)
        self.archer = Archer(context)
        self.spy = Spy(context)
        self.light = Light(context)
        self.marcher = Marcher(context)
        self.heavy = Heavy(context)
        self.ram = Ram(context)
        self.catapult = Catapult(context)
        self.knight = Knight(context)
        # self.snob = Snob(context)
        self.militia = Militia(context)

    def get_units(self):
        return [self.spear, self.sword, self.axe, self.archer, self.spy, self.light, self.marcher, self.heavy, self.ram,
                self.catapult, self.knight, self.militia]  # snob!


class TechLevel(object):
    def __init__(self, w, s, i, a, di, dc, da):
        self.wood_cost = w
        self.stone_cost = s
        self.iron_cost = i
        self.attack_factor = a
        self.def_inf_factor = di
        self.def_cav_factor = dc
        self.def_arc_factor = da


class UnitBase(object):
    def __init__(self, context):
        self.context = context
        self.error_code = 0
        self.amount = 0

    def check_requirements(self, bot): #todo move to other module?
        if self.error_code == 1 or self.error_code == 2:  # 1=Build dependency, 2=Tech dependency
            yield self.context.env.process(bot.builder.build_dependencies(self))
            # yield module.env.process(module.session.smith_research(module, bot, self))

    def cost(self):
        return self.wood_cost, self.stone_cost, self.iron_cost

    @property
    def wood_cost(self):
        return self.wood * self.amount

    @wood_cost.setter
    def wood_cost(self, value):
        self.wood = value

    @property
    def stone_cost(self):
        return self.stone * self.amount

    @stone_cost.setter
    def stone_cost(self, value):
        self.stone = value

    @property
    def iron_cost(self):
        return self.iron * self.amount

    @iron_cost.setter
    def iron_cost(self, value):
        self.iron = value

    @property
    def loot(self):
        return self._loot * self.amount

    @loot.setter
    def loot(self, value):
        self._loot = value

    def actual_speed(self):
        return self.speed_base / (self.context.world_config.get_float('config.speed')
                                  * self.context.world_config.get_float('config.unit_speed'))

    def actual_build_time(self, level):
        # actual build time of a unit = 2/3*[build_time]*1.06^(-[level of the building the unit is created in])
        return math.pow(1.06, -level) * 2 / 3 * (self.build_time / self.context.world_config.get_float('config.speed'))

    # def actual_population(self):
    #    return self.pop * self.available.level

    def attack_power(self):
        return self.amount * self.attack

    def __str__(self):
        return "unit"


class Infantry(UnitBase):
    def __init__(self, context):
        super(Infantry, self).__init__(context)
        self.techs = list()
        self.tech_level = 0

    def __str__(self):
        return "infantry"


class Cavalry(UnitBase):
    def __init__(self, context):
        super(Cavalry, self).__init__(context)
        self.techs = list()
        self.tech_level = 0

    def __str__(self):
        return "cavalry"


class Siege(UnitBase):
    def __init__(self, context):
        super(Siege, self).__init__(context)
        self.techs = list()
        self.tech_level = 0

    def __str__(self):
        return "siege"


class Spear(Infantry):
    wood = 50
    stone = 30
    iron = 10
    pop = 1
    speed_base = 18
    attack = 10
    def_inf = 15
    def_cav = 45
    def_arc = 20
    loot = 25
    build_time = 1020
    deps = {"main": 3, "barracks": 1}
    use = [1, 6]  # 0=Aanvallen, 1=Verdedigen, 2=Scouten, 4=Slopen, 5=Edelen, 6=Looten, 7=Speed
    tech_level = 1
    techs = [TechLevel(0, 0, 0, 1, 1, 1, 1),
             TechLevel(2560, 2240, 2960, 1.3, 19 / 15, 56 / 45, 1.25),
             TechLevel(10240, 8960, 11840, 1.4, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "spear"


class Sword(Infantry):
    wood = 30
    stone = 30
    iron = 70
    pop = 1
    speed_base = 22
    attack = 25
    def_inf = 50
    def_cav = 15
    def_arc = 40
    loot = 15
    build_time = 1500
    deps = {"main": 5, "barracks": 1}
    use = [1, 6]
    tech_level = 1
    techs = [TechLevel(0, 0, 0, 1, 1, 1, 1),
             TechLevel(3600, 3200, 3120, 1.24, 1.26, 19 / 15, 1.25),
             TechLevel(14400, 12800, 12480, 1.4, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "sword"


class Axe(Infantry):
    wood = 60
    stone = 30
    iron = 40
    pop = 1
    speed_base = 18
    attack = 40
    def_inf = 10
    def_cav = 5
    def_arc = 10
    loot = 10
    build_time = 1320
    deps = {"smith": 2}
    use = [0]
    tech_level = 0
    techs = [TechLevel(700, 840, 820, 1, 1, 1, 1),
             TechLevel(2800, 3360, 3280, 1.25, 1.3, 1.2, 1.3),
             TechLevel(11200, 13440, 13120, 1.4, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "axe"


class Archer(Infantry):
    wood = 100
    stone = 30
    iron = 60
    pop = 1
    attack = 15
    def_inf = 50
    def_cav = 40
    def_arc = 5
    speed_base = 18
    loot = 10
    build_time = 1800
    deps = {"barracks": 5, "smith": 5}
    use = [1]
    tech_level = 0
    techs = [TechLevel(640, 560, 740, 1, 1, 1, 1),
             TechLevel(2560, 2240, 2960, 19 / 15, 1.26, 1.25, 1.2),
             TechLevel(10240, 8960, 11840, 1.4, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "archer"


class Spy(Infantry):
    wood = 50
    stone = 50
    iron = 20
    pop = 2
    attack = 0
    def_inf = 2
    def_cav = 1
    def_arc = 2
    speed_base = 9
    loot = 0
    build_time = 900
    deps = {"stable": 1}
    use = [2]
    tech_level = 0
    techs = [TechLevel(560, 480, 480, 1, 1, 1, 1),
             TechLevel(2240, 1920, 1920, 1, 1.5, 1, 1),
             TechLevel(8960, 7680, 7680, 1, 1.5, 1, 1.5)]

    def __str__(self):
        return "spy"


class Light(Cavalry):
    wood = 125
    stone = 100
    iron = 250
    pop = 4
    attack = 130
    def_inf = 30
    def_cav = 40
    def_arc = 30
    speed_base = 10
    loot = 80
    build_time = 1800
    deps = {"stable": 3}
    use = [0, 7]
    tech_level = 0
    techs = [TechLevel(2200, 2400, 2000, 1, 1, 1, 1),
             TechLevel(8800, 9600, 8000, 163 / 130, 38 / 30, 1, 1),
             TechLevel(35200, 38400, 32000, 1.4, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "light"


class Marcher(Cavalry):
    wood = 250
    stone = 100
    iron = 150
    pop = 5
    attack = 120
    def_inf = 40
    def_cav = 30
    def_arc = 50
    speed_base = 10
    loot = 50
    build_time = 2700
    deps = {"stable": 5}
    use = [0, 7] # 0=Aanvallen, 1=Verdedigen, 2=Scouten, 4=Slopen, 5=Edelen, 6=Looten, 7=Speed
    tech_level = 0
    techs = [TechLevel(3000, 2400, 2000, 1, 1, 1, 1),
             TechLevel(12000, 9600, 8000, 1.25, 1.25, 38 / 30, 1.26),
             TechLevel(48000, 38400, 32000, 1.4, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "marcher"


class Heavy(Cavalry):
    wood = 200
    stone = 150
    iron = 600
    pop = 6
    attack = 150
    def_inf = 200
    def_cav = 80
    def_arc = 180
    speed_base = 11
    loot = 50
    build_time = 3600
    deps = {"stable": 10, "smith": 15}
    use = [0, 1, 7]
    tech_level = 0
    techs = [TechLevel(3000, 2400, 2000, 1, 1, 1, 1),
             TechLevel(12000, 9600, 8000, 180 / 150, 1.25, 1.25, 1.25),
             TechLevel(48000, 38400, 32000, 1.4, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "heavy"


class Ram(Siege):
    wood = 300
    stone = 200
    iron = 200
    pop = 5
    attack = 2
    def_inf = 20
    def_cav = 50
    def_arc = 20
    speed_base = 30
    loot = 0
    build_time = 4800
    use = [0, 4]
    deps = {"garage": 1}
    tech_level = 0
    techs = [TechLevel(1200, 1600, 800, 1, 1, 1, 1),
             TechLevel(4800, 6400, 3200, 1.5, 1.25, 1.26, 1.25),
             TechLevel(19200, 25600, 12800, 1.5, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "ram"


class Catapult(Siege):
    wood = 320
    stone = 400
    iron = 100
    pop = 8
    attack = 100
    def_inf = 100
    def_cav = 50
    def_arc = 100
    speed_base = 30
    loot = 0
    build_time = 7200
    deps = {"garage": 2, "smith": 12}
    use = [0, 1, 4]
    tech_level = 0
    techs = [TechLevel(1600, 2000, 1200, 1, 1, 1, 1),
             TechLevel(6400, 8000, 4800, 1.25, 1.25, 1.26, 1.25),
             TechLevel(25600, 32000, 19200, 1.4, 1.4, 1.4, 1.4)]

    def __str__(self):
        return "catapult"


class Knight(UnitBase):
    wood = 20
    stone = 20
    iron = 40
    pop = 10
    attack = 150
    def_inf = 250
    def_cav = 400
    def_arc = 150
    speed_base = 16
    loot = 100
    build_time = 21600
    deps = {"statue": 1}
    use = [0, 1, 6, 7]

    def __str__(self):
        return "knight"


class Snob(UnitBase):
    wood = 40000
    stone = 50000
    iron = 50000
    pop = 100
    attack = 30
    def_inf = 100
    def_cav = 50
    def_arc = 100
    speed_base = 56
    loot = 0
    build_time = 18000
    deps = {"snob": 1, "main": 20, "smith": 20, "market": 10}
    use = [5]

    def __str__(self):
        return "snob"


class Militia(UnitBase):
    wood = 0
    stone = 0
    iron = 0
    pop = 0
    attack = 5
    def_inf = 15
    def_cav = 45
    def_arc = 25
    speed_base = 0.02
    loot = 0
    build_time = 1
    deps = {"farm": 1}
    use = [1]

    def __str__(self):
        return "militia"
