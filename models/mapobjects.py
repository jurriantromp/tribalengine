import math
from operator import methodcaller

from agents.village.builder import Builder
from agents.village.treasurer import Treasurer
from agents.village.recruiter import Recruiter
from agents.village.captain import Captain
from agents.village.mayor import Mayor
from models.building import Buildings
from models.unit import Units
from misc.env import CustomContainer


class MapObject(object):
    def __init__(self, context, x, y):
        self.context = context
        self.x = x
        self.y = y

    def get_distance(self, x, y):
        xa = abs(self.x - x)
        ya = abs(self.y - y)
        distance = math.sqrt(xa * xa + ya * ya)
        return distance

    def get_object_distance(self, map_object):
        assert isinstance(map_object, MapObject)
        xa = abs(self.x - map_object.x)
        ya = abs(self.y - map_object.y)
        distance = math.sqrt(xa * xa + ya * ya)
        return distance

    def __repr__(self):
        return '<%s>' % self.__str__()

    def __str__(self):
        return '%s [%i, %i]' % (type(self).__name__, self.x, self.y)

    @property
    def type(self):
        return NotImplementedError


class Village(MapObject):
    def __init__(self, context, x, y, name=None, points=None):
        super(Village, self).__init__(context, x, y)
        self.id = None
        self.name = name
        self.points = points
        self.reports = list()


class ActorVillage(Village):
    def __init__(self, context, x, y, actor, *args, **kwargs):
        super(ActorVillage, self).__init__(context, x, y, **kwargs)

        # set actor
        self.actor = None
        self.set_owner(actor)

        # init agents
        self.mayor = Mayor(context, self)
        self.builder = Builder(context, self)
        self.recruiter = Recruiter(context, self)
        self.treasurer = Treasurer(context, self)
        self.captain = Captain(context)

        # init models
        # self.unit_groups = Units(context)
        # self.units = 10.0  # todo temp
        self.units = Units(context)
        self.buildings = Buildings(context, self)

        self.wood = CustomContainer(self.context.env,
                                    self.buildings.wood.production(),
                                    self.buildings.storage.capacity,
                                    500)

        self.stone = CustomContainer(self.context.env,
                                     self.buildings.stone.production(),
                                     self.buildings.storage.capacity,
                                     500)

        self.iron = CustomContainer(self.context.env,
                                    self.buildings.iron.production(),
                                    self.buildings.storage.capacity,
                                    500)

        print
        # call functions
        # self.refresh_all()
        # self.execute()

    def __str__(self):
        return '%s [%i, %i] %s' % (type(self).__name__, self.x, self.y, self.actor)

    def set_owner(self, actor):
        self.actor = actor
        actor.set_village(self.x, self.y, self)

    def remove_owner(self):
        actor = self.actor
        self.actor = None
        actor.remove_village(self.x, self.y)

    def refresh_buildings(self):
        construction_info = self.bot_player.session.post("main_construction_info", 'g', self.id)
        for building, properties in construction_info.get('buildings').items():
            getattr(self.buildings, building).set_level(int(properties.get('level')))

    def refresh_units(self):
        train_info = self.bot_player.session.post("train_building", 'g', self.id, "all")
        for name, properties in train_info.get('options').items():
            unit = getattr(self.units, name)
            unit.available._level = int(properties.get('available'))
            unit.amount = int(properties.get('all_count'))
            unit.error_code = properties.get('error_code')
            unit.error = properties.get('error')

    def refresh_village_data(self):
        data = self.bot_player.session.post("village_data", 'g', self.id)
        self.buildings.storage.wood_store._level = data.get('wood')
        self.buildings.storage.stone_store._level = data.get('stone')
        self.buildings.storage.iron_store._level = data.get('iron')

    def refresh_map(self):
        from models.worldmap import get_villages
        player_villages = get_villages(self)
        self.local_villages = sorted(player_villages.values(), key=methodcaller('get_distance', self.x, self.y))[
                              :100]

    def refresh_all(self):
        self.refresh_buildings()
        self.refresh_units()
        self.refresh_village_data()
        self.refresh_map()

    def execute(self):
        # logger.info("Execute processes")
        self.context.env.process(self.treasurer.production())
        # env.process(recruit_units(module, bot))
        # start_delayed(env, recruit_units(), 3000)
        self.context.env.process(self.builder.upgrade_resources())
        # env.process(captain())
        # env.process(raider(module, bot))
        ###start_delayed(env, architect(module, bot), 1)
        # print 'abc'
        # if not DRY:
        #    env.process(heartbeat())

    def get_actual_population(self):
        #return sum(building.actual_population() for building in self.buildings.get_buildings()) + \
        #       sum(units.actual_population() for units in self.units.get_units())
        return 1  # todo

    def get_actual_points(self):
        return sum(building.actual_points() for building in self.buildings.get_buildings())

    def attack_power(self):
        return sum(unit.attack_power() for unit in self.units.get_units())


class BotVillage(ActorVillage):
    type = 0

    def __init__(self, *args, **kwargs):
        super(BotVillage, self).__init__(*args, **kwargs)


class PlayerVillage(Village):
    type = 1

    # def get_weighted_gain(self):
    #     #todo fade old reports
    #     loot = sum(report.loot() for report in self.reports)
    #     loss = sum(report.loss() for report in self.reports)
    #     delta = loot - loss
    #     if delta > 0:
    #         return loot - loss / self.distance * 2
    #     else:
    #         return loot - loss * self.distance * 2
    #
    # def get_loss_ratio(self):
    #     sum(report.loot() for report in self.reports)


class Barbarian(Village):
    type = 2


class Bonus(Village):
    type = 3


class Cave(MapObject):
    type = 4


class Scenery(MapObject):
    type = 5
