import sys
from threading import Thread

import numpy as np
import simpy

from config import ACCOUNTS
from interface import environment
from misc.env import CustomEnvironment
import misc.logger as logger
from models.worldconfig import WorldConfig
from models.worldmap import WorldMap
from gui import PyGameWorld


class TribalEngine(object):

    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/var/log/tribalengine/info.log'  # '/dev/tty'
        self.stderr_path = '/var/log/tribalengine/error.log'  # '/dev/tty'
        self.pidfile_path = '/var/run/tribalengine.pid'
        self.pidfile_timeout = 5

    def run(self, modus):
        logger.logger.info("Starting %s server - initiating controller.." % modus)

        if modus == 'online':
            for world, accounts in ACCOUNTS.items():
                for player_name in accounts["ACCOUNTS"].keys():
                    p = Thread(target=simulate_online, args=(world, player_name,))
                    p.daemon = True
                    p.name = world + '_' + player_name
                    p.start()
        elif modus == 'training':
            simulate_training()
        else:
            raise NotImplementedError


def simulate_online(world, player_name):
    logger.logger.info('Start online simulation...')

    # if world not in variables.world_config:
    #     variables.world_config[world] = WorldConfig(world)
    #     variables.local.world_config = variables.world_config[world]
    #
    # variables.local.world = world
    #
    # bot_player = BotPlayer(player_name)
    # variables.bot_players[world] = bot_player # todo FIX THIS! need an id for the botplayer
    #
    # while True:
    #     variables.env.step()


class SimulationContext(object):
    pass


def simulate_training():
    logger.logger.info('Start training simulation...')

    world = None
    try:
        world = sys.argv[2]
    except IndexError:
        exit("Usage main.py training <world>")

    context = SimulationContext()
    context.env = CustomEnvironment()

    logger.set_adapter(context.env)
    reload(logger)

    context.interface = environment.TrainingInterface(context)

    context.world_config = WorldConfig(world)
    # b = world_config.get('config.buildings.custom_barracks')

    matrix = [
        ['  ', '  ', '  ', '  ', '  ', 'Bo', '  ', '  ', '  '],

        ['  ', '  ', '  ', '  ', '  ', '  ', '  ', '  ', '  '],

        ['  ', '  ', 'BP', '  ', '^ ', '^ ', '  ', 'Ba', '  '],

        ['  ', '  ', '  ', '^ ', '  ', '  ', '  ', '  ', '  '],

        ['  ', '  ', '  ', '  ', 'BP', '^ ', '  ', '  ', '  '],

        ['  ', '  ', '  ', '  ', '  ', '  ', '  ', '  ', '  '],

        ['  ', 'BP', '  ', '  ', '^ ', '  ', '^ ', 'BP', '  '],

        ['  ', '  ', '  ', '  ', 'ca', '  ', '  ', '  ', '  '],

        ['  ', '  ', '  ', '  ', '  ', '  ', '  ', '  ', '  '],
    ]

    context.world_map = WorldMap.from_matrix(context, matrix)

    villages = context.world_map.get_actor_villages()

    # gui = PyGameWorld(context)

    # bot_player = BotPlayer(player_name)

    def each_step(village):
        available_functions = {
            'upgrade_production': 0.5,
            'train_unit': 0.49,
            'attack_target': 0.01,
        }

        done = False
        while not done:
            choices = np.random.choice(available_functions.keys(), size=1000000, p=available_functions.values())
            for choice in np.nditer(choices):
                method = getattr(village.mayor, str(choice))
                try:
                    yield context.env.process(method())
                except simpy.Interrupt, i:
                    logger.logger.info(i)
                    # done = True

                    available_functions = {
                        'train_unit': 0.9,
                        'attack_target': 0.1,
                    }

                    break

    for village in villages:
        # context.env.process(village.treasurer.production())
        context.env.process(each_step(village))

    prev = -1
    while True:
        # gui.update()
        # print context.env.now
        context.env.step()

        if prev != context.env.now:
            prev = context.env.now
            active_players = list()
            for player in context.world_map.players:
                if player.has_villages:
                    active_players.append(player)

            if len(active_players) == 1:
                break

    print "Player %s is the winner!" % str(id(active_players[0]))[-3:]
    print "Now: ", context.env.now


if __name__ == "__main__":
    argv = sys.argv[1:]

    controller = TribalEngine()
    try:
        if argv[0] == 'training':
            controller.run('training')
        elif argv[0] == 'online':
            controller.run('online')
        else:
            raise RuntimeError()
    except (IndexError, RuntimeError):
        exit("Usage main.py <online|training>")
