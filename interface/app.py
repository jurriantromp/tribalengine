# todo broken (server problem)

import logging
import gevent
from socketio import socketio_manage
from socketio.namespace import BaseNamespace

from misc.logger import logger, SocketIOHandler
import variables


villages = dict()


def get_all_villages():
    if not villages:
        for player in variables.bot_players.values():
            villages.update(player.bot_villages.items())
    return villages


class InterfaceNamespace(BaseNamespace):
    def initialize(self):
        ih = SocketIOHandler(self)
        ih.setLevel(logging.DEBUG)
        clean_format = logging.Formatter('%(message)s')
        ih.setFormatter(clean_format)
        logger.addHandler(ih)

    def _heartbeat(self):
        while True:
            data = dict()
            for id, v in get_all_villages().items():
                data[id] = {'name': v.name, 'points': v.get_actual_points(),
                            'wood': int(v.buildings.storage.wood_store.level),
                            'stone': int(v.buildings.storage.stone_store.level),
                            'iron': int(v.buildings.storage.iron_store.level),
                            'store': v.buildings.storage.wood_store.capacity, 'pop': int(v.get_actual_population()),
                            'max_pop': v.buildings.farm.capacity(), 'main_queue': v.buildings.main.queue.get_queue}
            self.emit('heartbeat', data)
            gevent.sleep(10)

    def on_build(self, vid, building, prio=0, level=0):
        bot_village = get_all_villages()[int(vid)]
        building = getattr(bot_village.buildings, building)
        variables.env.process(bot_village.builder.build(building, int(prio), int(level)))

    def on_train(self, vid, unit, amount):
        bot_village = get_all_villages()[int(vid)]
        unit = getattr(bot_village.units, unit)
        variables.env.process(bot_village.recruiter.recruit(unit, int(amount)))

    def on_command(self, vid, x, y, target_village, type, spear, sword, archer, axe, spy, light, marcher, heavy, ram,
                   catapult, knight, snob):
        bot_village = get_all_villages()[int(vid)]

        def _int(s):
            s = s.strip()
            return int(s) if s else 0

        if x and y:
            bot_village.captain.command_coordinates(bot_village.session, _int(x), _int(y), type, _int(spear),
                                                    _int(sword), _int(archer), _int(axe), _int(spy), _int(light),
                                                    _int(marcher), _int(heavy), _int(ram), _int(catapult), _int(knight),
                                                    _int(snob))
        else:
            village = bot_village.local_villages[
                _int(target_village)]  # todo change to all instead of only local villages
            bot_village.captain.command(bot_village.session, village, type, _int(spear), _int(sword), _int(archer),
                                        _int(axe), _int(spy), _int(light), _int(marcher), _int(heavy), _int(ram),
                                        _int(catapult), _int(knight), _int(snob))

    def recv_connect(self):
        logger.debug('Socket connected')
        self.spawn(self._heartbeat)


def interface(environ, start_response):
    socketio_manage(environ, {'/stream': InterfaceNamespace})