from misc.logger import logger


class AbstractInterface(object):
    def village_attack_target(self, origin_village, target_village, unit_group):
        self.send_troops(unit_group)
        self.calculate_losses(unit_group, target_village.unit_groups)

    def send_troops(self, unit_group):
        raise NotImplementedError

    def calculate_losses(self, origin_village, target_village):
        raise NotImplementedError


class OnlineInterface(AbstractInterface):
    pass


class TrainingInterface(AbstractInterface):
    def __init__(self, context):
        self.env = context.env
        self.context = context

    def send_troops(self, unit_group):
        # Abstraction of OnlineInterface
        pass

    def calculate_losses(self, origin_unit_group, target_unit_groups):
        for target_unit_group in target_unit_groups:
            origin_unit_group -= target_unit_group

    def wait_until_available(self, village, upgrade_item):
        return self.env.process(
            village.treasurer.treasury_get(
                upgrade_item.wood,
                upgrade_item.stone,
                upgrade_item.iron,
            )
        )

    def upgrade_production(self, village):
        return village.builder.upgrade_production()

    def train_unit(self, village):
        return village.recruiter.recruit(village.units.spear)

    def village_attack_target(self, origin_village, target_village=None, unit_group=None):
        for village in self.context.world_map.get_bot_villages():
            if village.actor != origin_village.actor:
                target_village = village
                break

        assert target_village

        defense_multiplier = 1.2
        target_survivors = round(target_village.units.spear.amount * defense_multiplier - origin_village.units.spear.amount)

        if target_survivors >= 0:
            # Target village wins or draw
            target_village.units.spear.amount = target_survivors
            origin_village.units.spear.amount = 0
        else:
            # Origin village wins
            target_village.units.spear.amount = 0
            origin_village.units.spear.amount = round(origin_village.units.spear.amount - target_village.units.spear.amount * defense_multiplier)
            logger.info("%s wins %s" % (origin_village, target_village))
            target_village.remove_owner()
            target_village.set_owner(origin_village.actor)

        yield self.env.timeout(10000)
