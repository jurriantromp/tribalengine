import requests
import json
import pickle
import hashlib
import os.path
import time
from collections import namedtuple

import variables
from config import ACCOUNTS, SESSION_TIME
from constants import MOBILE_HEADERS, HTML_HEADERS
from misc.logger import logger


class LoginError(Exception):
    def __init__(self, value=''):
        self.value = value

        if value == u"Om nog meer bouwopdrachten te kunnen geven heb je een Premium Account nodig":
            pass
        elif value == u"World unavailable":
            exit(value)
        elif value == u"Deze functie is niet beschikbaar als je account in slaapmodus zit.":
            exit(value)
        else:
            exit(value)


    def __str__(self):
        return repr(self.value)


class SessionManager(object):
    def __init__(self, username):
        self.username = username
        self.password = ACCOUNTS[variables.local.world]['ACCOUNTS'][username]
        self.sid = None
        self.s = requests.session()
        self.s.headers = MOBILE_HEADERS
        self.hs = requests.session()
        self.hs.headers = HTML_HEADERS
        self.session_cache = "tmp/" + variables.local.world + '_' + self.username + ".sid"

    @staticmethod
    def get_hash(data):
        return hashlib.sha1(b"2sB2jaeNEG6C01QOTldcgCKO-" + data).hexdigest()

    def post(self, action, type='g', *args):
        if action in ['login', 'push_token']:
            data = '["' + '", "'.join(str(item) for item in args) + '"]'
        else:
            data = '["' + self.sid + '", "' + '", "'.join(str(item) for item in args) + '"]'

        if type == 'm':
            server = 'www'
        else:
            server = variables.local.world

        url = "http://%s.%s/m/%s/%s?hash=%s" % (server, ACCOUNTS[variables.local.world]['DOMAIN'], type, action, self.get_hash(data))
        response = self.s.post(url, data=data)

        if response.text:
            json_data = json.loads(response.text)
            if json_data.get(u'invalidsession'):
                logger.debug("Invalid session, login again")
                self.login(force=True)
                return self.post(action, type, *args)

            if json_data.get(u'error'):
                raise LoginError(json_data.get(u'error'))
            elif json_data.get('result') == list():
                logger.critical('Session: empty list result')
                return #todo empty list result, temporary fix
            elif json_data.get('result'):
                return json_data.get('result')
            else:
                raise Exception("Request returned odd json: %s" % json_data)
        raise Exception("Request returned no valid POST response")

    def html(self, get, type='g', post=None):
        query = 'm.php?' if not type == 'g' else 'game.php?'
        query += '&'.join("%s=%s" % (k, v) for k, v in get.items())
        url = "http://%s.%s/%s" % (variables.local.world, ACCOUNTS[variables.local.world]['DOMAIN'], query)

        if post:
            response = self.hs.post(url, data=post, allow_redirects=True)
        else:
            response = self.hs.get(url, allow_redirects=True)

        self.status_code = response.status_code # todo temp
        if response.text:
            return response.text
        raise Exception("Request returned no valid response")

    def login(self, force=False):
        is_cached = ''
        if not force and os.path.isfile(self.session_cache) and time.time() - os.path.getmtime(self.session_cache) < SESSION_TIME:
            file = open(self.session_cache, 'r')
            self.sid = file.read()
            is_cached = ' using cache'
        else:
            try:
                token = self.post("login", 'm', self.username, self.password, 1035).get("token")

                self.post('push_token', 'm', token, "5288ad9b63a692d323b83cff45e3062843e83130a43661153799e72025461de4",
                          "D869A7F8-1DEE-485D-AFBE-7AC0EBA4557A", "iPhone 3GS 6.1.3")

                self.sid = self.post('login', 'g', token, "2").get("sid")

                file = open(self.session_cache, 'wb')
                file.write(self.sid)

                self.hs.post("http://%s.%s/login.php?mobile&sid=%s&2" % (
                variables.local.world, ACCOUNTS[variables.local.world]['DOMAIN'], self.sid), allow_redirects=False)
            except (AttributeError, ValueError):
                raise Exception("Login failed!")

        logger.info('Logged in%s..' % is_cached)

    #todo move away to other module?
    def villages_get(self):
        villages = self.post("villages_get", 'g')
        result = dict()
        Village = namedtuple('Village', 'id name x y points')
        for v in villages:
            result[int(v[0])] = Village(int(v[0]), v[3], int(v[1]), int(v[2]), int(v[4]))
        return result