# all accounts that are used, separate process per world
ACCOUNTS = {
    "nl62": {
        "DOMAIN": "tribalwars.nl",
        "ACCOUNTS": {
            "yazilim": "d2L7sR3e"  # Turks voor software
        }
    }
}

# simulate a dry run: default False
DRY = False

# using the same http session for: default 30 min
SESSION_TIME = 1800