import pygame
import sys


class PyGameWorld(object):
    def __init__(self, context):
        self.context = context
        pygame.init()

        self.grid_size = 32
        display_width = self.grid_size * self.context.world_map.width
        display_height = self.grid_size * self.context.world_map.height
        self.screen = pygame.display.set_mode((display_width, display_height))

        self.bg = 96, 126, 12

        self.rect = pygame.Surface((self.grid_size, self.grid_size,))

    def setup_world(self):
        matrix = dict()
        for map_object in self.context.world_map.map_objects:
            try:
                matrix[map_object.x][map_object.y] = map_object
            except KeyError:
                matrix[map_object.x] = {map_object.y: map_object}

        for i in range(0, self.context.world_map.width):
            for j in range(0, self.context.world_map.height):
                try:
                    item = matrix[i][j]
                except KeyError:
                    continue
                if item.type == 0:
                    color = 255, 255, 255
                elif item.type == 1:
                    color = 130, 60, 10
                elif item.type == 2:
                    color = 150, 150, 150
                elif item.type == 3:
                    color = 139, 5, 109
                elif item.type == 4:
                    color = 0, 94, 117
                elif item.type == 5:
                    color = 70, 103, 21
                else:
                    raise Exception('Unknown')

                self.rect.fill(color)

                self.screen.blit(self.rect, (i * self.grid_size, j * self.grid_size))

    def update(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        self.screen.fill(self.bg)
        self.setup_world()
        pygame.display.flip()