(function () {

    // Create and connect sockert
    var socket = io.connect('http://localhost:8080/stream');

    // Grab some references
    var $status = $('#status');

    // Bind the login form
    $('#build').bind('submit', function () {
        //alert();
        socket.emit('build', $('#generic-bot').val(), $('#build-building').val(), $('#build-prio').val(), $('#build-level').val());
        return false;
    });

    $('#unit').bind('submit', function () {
        socket.emit('train', $('#generic-bot').val(), $('#unit-type').val(), $('#unit-amount').val());
        return false;
    });

    $('#command').bind('submit', function () {
        socket.emit('command', $('#generic-bot').val(), $('#x').val(), $('#y').val(), $('#village').val(), $('#type').val(), $('#spear').val(), $('#sword').val(), $('#archer').val(), $('#axe').val(), $('#spy').val(), $('#light').val(), $('#marcher').val(), $('#heavy').val(), $('#ram').val(), $('#catapult').val(), $('#knight').val(), $('#snob').val());
        return false;
    });


    // Bind events to the socket
    socket.on('connect', function () {
        $status.html('<b>Connected: ' + socket.socket.transport.name + '</b>');
    });
    socket.on('error', function () {
        $status.html('<b>Error</b>');
    });
    socket.on('disconnect', function () {
        $status.html('<b>Closed</b>');
    });
    socket.on('heartbeat', function (msg) {
        var table = '<table border="1" cellspacing="1">';
        table += '<tr><th>ID</th><th>Village</th><th>Wood</th><th>Stone</th><th>Iron</th><th>Store</th><th>Pop</th><th>Pop max</th><th>Points</th></tr>';
        var headers = '';
        for (var k in msg) {
            if (msg.hasOwnProperty(k)) {
                table += '<tr><td>' + k + '</td><td>' + msg[k]["name"] + '</td><td>' + msg[k]["wood"] + '</td><td>' + msg[k]["stone"] + '</td><td>' + msg[k]["iron"] + '</td><td>' + msg[k]["store"] + '</td><td>' + msg[k]["pop"] + '</td><td>' + msg[k]["max_pop"] + '</td><td>' + msg[k]["points"] + '</td></tr>';
                //console.log("Key is " + k + ", value is" + msg[k]['pop']);
            }
        }
        table += '</table>';
        $('#heartbeat').html(table);
    });


    function fake_load() {
        var cur_value = 1,
            progress;

        // Make a loader.
        var loader = new PNotify({
            //title: "Lowering the Moon",
            title: "Creating Series of Tubes",
            text: '<div class="progress progress-striped active" style="margin:0"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">\
            <span class="sr-only">0%</span></div></div>',
            //icon: 'fa fa-moon-o fa-spin',
            icon: 'fa fa-cog fa-spin',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            },
            before_open: function (PNotify) {
                progress = PNotify.get().find("div.progress-bar");
                progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
                // Pretend to do something.
                var timer = setInterval(function () {
                    //if (cur_value == 50) {
                    //	loader.update({title: "Raising the Sun", icon: "fa fa-sun-o fa-spin"});
                    //}
                    if (cur_value >= 100) {
                        // Remove the interval.
                        window.clearInterval(timer);
                        //loader.remove();
                        return;
                    }
                    cur_value += 1;
                    progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
                }, 65);
            }
        });
    }

    fake_load();

})();
