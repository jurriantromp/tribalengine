HIGH_PRIO = -1
LOW_PRIO = 1

MOBILE_HEADERS = {
    "Accept-Encoding": "deflate, gzip",
    "Accept": "text/xml, application/xml, application/xhtml+xml, text/html;q=0.9, text/plain;q=0.8, text/css, \
        image/png, image/jpeg, image/gif;q=0.8, application/x-shockwave-flash, video/mp4;q=0.9, \
        flv-application/octet-stream;q=0.8, video/x-flv;q=0.7, audio/mp4, application/futuresplash, */*;q=0.5",
    "User-Agent": "Mozilla/5.0 (Android; U; nl-NL) AppleWebKit/533.19.4 (KHTML, like Gecko) AdobeAIR/3.7",
    "x-flash-version": "11,7,700,224",
    "Connection": "Keep-Alive",
    "Cache-Control": "no-cache",
    "Referer": "app:/Staemme.swf",
    "Content-Type": "flv-application/octet-stream; charset=UTF-8",
    "IGMobileDevice": "Android",
}

HTML_HEADERS = {
    "Connection": "keep-alive",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "User-Agent": "Mozilla/5.0 (Linux; U; Android 4.2.2; nl-nl; GT-I9195 Build/JDQ39) AppleWebKit/534.30 \
        (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
    "Accept-Encoding": "gzip,deflate",
    "Accept-Language": "nl-NL, en-US",
    "Accept-Charset": "utf-8, iso-8859-1, utf-16, *;q=0.7",
}