from unittest import TestCase
from misc.env import CustomEnvironment, CustomContainer, InsufficientCapacity


class BuildingTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    # def test_aaa(self):
    #     print 'aaaargh'
    #     env = simpy.Environment()
    #     container = simpy.Container(env, 1000, 500)
    #
    #     def container_get():
    #         yield container.get(999)
    #
    #
    #     env.process(container_get())
    #     env.run(until=100)
    #     while True:
    #         container.put(500)
    #         env.step()
    #
    #
    #     print

    def test_container_get(self):
        env = CustomEnvironment()
        container = CustomContainer(env, 0.2, lambda: 1000, 500)

        def container_get():
            yield container.get(400)
            yield container.get(400)

        env.process(container_get())

        env.run(until=1500)
        assert len(env._queue) == 1
        assert container.level == 400
        env.run(until=1501)
        assert len(env._queue) == 0
        assert container.level == 0.2

    def test_insufficient_capacity(self):
        env = CustomEnvironment()
        container = CustomContainer(env, 0.2, lambda: 1000)

        def container_get():
            with self.assertRaises(InsufficientCapacity):
                yield container.get(1100)

        env.process(container_get())
        env.run()

    def test_container_overflow(self):
        env = CustomEnvironment()
        container = CustomContainer(env, 100, lambda: 1000, 900)

        def container_get():
            yield container.get(500)

        env.run(until=2)
        assert container.level == 1000
        env.process(container_get())
        env.step()
        assert container.level == 500
        env.run(until=10)
        assert container.level == 1000

    def test_container_concurrent_get(self):
        env = CustomEnvironment()
        container = CustomContainer(env, 0.2, lambda: 1000, 500)

        def container_get():
            container.get(400)
            container.get(400)
            container.get(400)
            container.get(400)
            yield env.timeout(0)

        env.process(container_get())

        env.run()
        assert env.now == 5500

    def test_container_upgrade(self):
        env = CustomEnvironment()
        container = CustomContainer(env, 0.2, lambda: 1000, 500)

        def container_get():
            container.get(400)
            container.get(400)
            container.get(400)
            yield env.timeout(0)

        env.process(container_get())
        env.run(until=100)
        assert env._queue[0][0] == 1500
        container.linear_constant = 0.5
        assert env._queue[0][0] == 660
        assert container.level == 120
        env.run()
        assert env._now == 1460

    def test_container_put(self):
        pass