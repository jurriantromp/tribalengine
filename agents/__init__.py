class Agent(object):
    def __init__(self, context):
        self._interface = context.interface

    @property
    def interface(self):
        return self._interface

    @interface.setter
    def interface(self, value):
        self._interface = value


class Command(object):
    def __init__(self, target, success, failure, expires=None):
        pass

    def on_failure(self):
        pass

    def on_success(self):
        pass
