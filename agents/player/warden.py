# Civil commander of all the villages of a player
from agents import Agent
from agents.village.mayor import Mayor
from agents.village.captain import Captain


class Warden(Agent):
    ATTACK_STRATEGIES = {
        'clear_and_conquer': 1,
        'scout_and_clear': 2,
    }
    _attack_strategy = ATTACK_STRATEGIES['scout_and_clear']

    def conquer_region(self):
        # Try to conquer villages close on the map
        pass

    def conquer_village(self):
        #   - target_village
        #   - coordinate_attack
        #       - attack_target
        #       - conquer_target

        pass

    def target_village(self):
        pass

    @property
    def attack_strategy(self):
        return self._attack_strategy

    @attack_strategy.setter
    def attack_strategy(self, attack_strategy):
        assert attack_strategy in self.ATTACK_STRATEGIES
        self._attack_strategy = attack_strategy

    def coordinate_attack(self):
        for village in context.world_map.get_bot_villages():
            village.captain.attack_target(self._attack_strategy)

    def conquer_target(self):
        pass

    def aggressiveness(self):
        # Determine how aggressive the bot will pursue its goal
        # Trigger Event: game_starts
        pass

    def appoint_marcher(self):
        # Appoint marcher village specialized to defend vulnerable villages
        # Trigger Event: new_village_conquered
        pass

    def appoint_noble(self):
        # Appoint noble village specialized in conquering villages
        # Trigger Event: new_village_conquered
        pass

    def appoint_knight(self):
        # Appoint knight village specialized in attacking villages
        # Trigger Event: new_village_conquered
        pass
