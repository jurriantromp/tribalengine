# Supreme commander of all subject players
from agents import Agent, Command
from agents.player.warden import Warden


class Event(object):
    def __init__(self, event_name):
        try:
            self.event = self.events[event_name]
        except KeyError:
            raise Exception("Event %s not found", event_name)

class TribeEvent(Event):
    events = {
        'world_conquered': {
            'requirements': {
                'enemy_villages': 0
            }
        },
        'lost_own_villages': {
            'requirements': {
                'own_villages': 0
            }
        }
    }

class VillageEvent(Event):
    events = {
        'village_under_attack': {
            'requirements': {
                'incoming_attack': ('>', 0)
            }
        }
    }


class King(Agent):
    class ConquerWorld(Command):
        # Try to conquer all villages on the map that don't belong to the tribe
        target = Warden
        action = 'ConquerRegion'
        success = Event('world_conquered')
        failure = Event('lost_own_villages')
