import simpy
from operator import itemgetter
import math

import variables
import models
from misc.utils import date
from interface.session import LoginError
from simpy import Interrupt
from constants import HIGH_PRIO, LOW_PRIO
from config import DRY
from misc.logger import logger


class Builder(object):

    def __init__(self, context, bot_village):
        self.context = context
        self.bot_village = bot_village

    def calculate_dependencies(self, item, deps={}, depth=0):
        if not deps:
            deps = dict()
        if hasattr(item, 'deps'):
            #procs = list()
            for dep, level in item.deps.items():
                target = getattr(self, dep)
                #if level > target.level:
                #procs.append(module.env.process(build(target, level=level)))
                if not target in deps or deps[target][0] < level:
                    deps[target] = [level, depth]

                if deps[target][1] < depth:
                    deps[target][1] = depth

                self.calculate_dependencies(self, target, deps, depth + 1)
            #yield simpy.events.AllOf(module.env, procs)
        #else:
        #yield module.env.exit()
        return deps

    def build_dependencies(self, building, prio=0, level=0):  #also for unit dependencies
        targets = list()
        deps = [(x[0], x[1][0]) for x in
                sorted(self.calculate_dependencies(building).iteritems(), key=lambda a: a[1][1], reverse=True)]
        #if level > 0:
        if isinstance(building, models.building.Building):  # don't append if instance is unit
            level = building.level if level == 0 else level
            if level >= building.level:
                deps.append((building, level + 1))
            else:
                logger.error('Building is already on this level')

        for target, lvl in deps:
            for x in range(target.level + 1, lvl + 1):
                targets.append(self.context.env.process(self.do_build(target, prio, x)))
        yield simpy.events.AllOf(self.context.env, targets)

    def build(self, building, prio=0, level=0):
        logger.debug("Trying to schedule building %s to lvl %d" % (building, building.level + 1))

        if not building.upgrade_requirements():
            logger.error("Building %s - requirements not met" % building)
            self.context.env.exit()

        level_bck = building.level
        building.level += 1
        wood = building.wood_cost
        stone = building.stone_cost
        iron = building.iron_cost

        if building.__str__() != 'storage' and (wood > self.bot_village.buildings.storage.capacity() or
                                                stone > self.bot_village.buildings.storage.capacity() or
                                                iron > self.bot_village.buildings.storage.capacity()):
            logger.warn("Storage too small for %d %d %d of building, high prio upgrade" % (
            building.wood_cost, building.stone_cost, building.iron_cost))
            building.level = level_bck
            yield self.context.env.process(self.build(self.bot_village.buildings.storage, HIGH_PRIO)) #  discard original task
        elif building.__str__() != "farm" and (
            self.bot_village.buildings.farm.capacity() <= self.bot_village.get_actual_population() + building.actual_population(True)):
            logger.warn("Farm too small for %s of building, high prio upgrade" % str(
                self.bot_village.get_actual_population() + building.actual_population(True)))
            building.level = level_bck
            yield self.context.env.process(self.build(self.bot_village.buildings.farm, HIGH_PRIO)) #  discard original task
        else:
            building.level = level_bck
            yield self.context.env.process(self.build_dependencies(building, prio, level))
            #yield module.self.context.env.process(do_build(module, self.bot_village, building, prio, level))



    uid = 0
    def do_build(self, building, prio, level):
        global uid
        level_bck = building.level
        building.level = level
        wood = building.wood_cost
        stone = building.stone_cost
        iron = building.iron_cost
        main_level = self.bot_village.buildings.main.level
        if building == self.bot_village.buildings.main:
            main_level = level_bck
        build_time = building.actual_build_time(main_level)
        building.level = level_bck

        request = self.bot_village.buildings.main.queue.request(prio) #todo Priority fix
        request.item = building
        #print "Requested %s main queue at %s" % (building, module.variables.env.now)
        yield request
        yield self.context.env.process(self.bot_village.treasurer.treasury_get(wood, stone, iron, prio))
        start = self.context.env.now
        logger.debug("Pushing the upgrade")
        #f not DRY:
        #    self.bot_village.bot_player.session.post("main_do_upgrade", 'g', self.bot_village.id, building)
        logger.info("%s building %s lvl %s, waiting %ds till %s" % (self.bot_village, building, building.level + 1, build_time, self.context.env.now + build_time))
        yield self.context.env.timeout(math.ceil(build_time))
        building.upgrade()
        logger.debug("Built %s lvl %d at %s (%ds)" % (building, building.level, date(self.context.env.now), self.context.env.now - start))
        self.bot_village.buildings.main.queue.release(request)

    def production_order(self):
        return sorted(
            [
                (self.bot_village.buildings.wood.production() * 1.0, self.bot_village.buildings.wood),
                (self.bot_village.buildings.stone.production() * 1.0, self.bot_village.buildings.stone),
                (self.bot_village.buildings.iron.production() * 1.5, self.bot_village.buildings.iron),
            ],
            key=itemgetter(0),
            reverse=True
        )

    def upgrade_production(self):
        order = self.production_order()
        while True:
            try:
                _, building = order.pop()
            except IndexError:
                raise Interrupt('All production is upgraded to max level')

            if building.level == building.max_level:
                continue

            yield self.context.env.process(self.build(building, LOW_PRIO))
            break
