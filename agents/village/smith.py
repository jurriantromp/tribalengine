import re

import variables
from constants import HIGH_PRIO



class Smith(object):

    def __init__(self, bot_village):
        self.bot_village = bot_village

    def research(self, unit):
        #GET /game.php?screen=smith&village=4732&_partial HTTP/1.1
        response = self.bot_village.session.html({'screen': 'smith', 'village': self.bot_village.id, '_partial': ''})
        #print response

        # smith moet wel klaar zijn
        h = re.search(r'&h=([^&]+)', response).group(1)
        #\\\/game.php?village=4732&ajaxaction=research&h=ef29&screen=smith

        if variables.local.world_config.tech == 2:
            tech_level = unit.techs[0]
            yield variables.env.process(self.bot_village.treasurer.treasury_get(self.bot_village, tech_level.wood_cost, tech_level.stone_cost, tech_level.iron_cost, HIGH_PRIO))

            response = self.bot_village.session.html(
                {'village': self.bot_village.id, 'ajaxaction': 'research', 'h': h, 'screen': 'smith', 'client_time': '1405005877'},
                'g', {'tech_id': unit.__str__(), 'source': self.bot_village.id})

            if response == u'{"error":"Technologie volledig ontwikkeld"}':
                #raise Exception("Technologie volledig ontwikkeld")
                return #todo temp

            t = re.search(r'timer\\">([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})', response)
            seconds = int(t.group(1)) * 3600 + int(t.group(2)) * 60 + int(t.group(3))
            if seconds:
                yield variables.env.timeout(seconds)