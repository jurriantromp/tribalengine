import simpy

import variables
from misc.utils import date
from misc.logger import logger


class Treasurer(object):

    def __init__(self, context, bot_village):
        self.context = context
        self.bot_village = bot_village


    def put_resources(self, wood_amount, stone_amount, iron_amount):
        if self.bot_village.buildings.storage.wood_store.capacity - self.bot_village.buildings.storage.wood_store.level - wood_amount > 0 and wood_amount > 0:
            self.bot_village.wood.put(wood_amount)
        elif wood_amount < 1 or self.bot_village.buildings.storage.wood_store.capacity - self.bot_village.buildings.storage.wood_store.level < 1:
            pass
        else:
            logger.warn("Overflow of wood in storage")
            self.bot_village.buildings.storage.wood_store.put(self.bot_village.buildings.storage.wood_store.capacity - self.bot_village.buildings.storage.wood_store.level)

        if self.bot_village.buildings.storage.stone_store.capacity - self.bot_village.buildings.storage.stone_store.level - stone_amount > 0 and stone_amount > 0:
            self.bot_village.buildings.storage.stone_store.put(stone_amount)
        elif stone_amount < 1 or self.bot_village.buildings.storage.stone_store.capacity - self.bot_village.buildings.storage.stone_store.level < 1:
            pass
        else:
            logger.warn("Overflow of stone in storage")
            self.bot_village.buildings.storage.stone_store.put(self.bot_village.buildings.storage.stone_store.capacity - self.bot_village.buildings.storage.stone_store.level)

        if self.bot_village.buildings.storage.iron_store.capacity - self.bot_village.buildings.storage.iron_store.level - iron_amount > 0 and iron_amount > 0:
            self.bot_village.buildings.storage.iron_store.put(iron_amount)
        elif iron_amount < 1 or self.bot_village.buildings.storage.iron_store.capacity - self.bot_village.buildings.storage.iron_store.level < 1:
            pass
        else:
            logger.warn("Overflow of iron in storage")
            self.bot_village.buildings.storage.iron_store.put(self.bot_village.buildings.storage.iron_store.capacity - self.bot_village.buildings.storage.iron_store.level)


    def production(self):
        while True:
            self.put_resources(self.bot_village.buildings.wood.production(), self.bot_village.buildings.stone.production(), self.bot_village.buildings.iron.production())
            yield self.context.env.timeout(1)


    def treasury_get(self, wood, stone, iron, prio=0):
        request = self.bot_village.buildings.storage.queue.request(prio)
        #request.item = None
        try:
            yield request
            logger.debug("Next cost: %d w %d s %d i %s" % (wood, stone, iron, date(self.context.env.now)))
            yield self.bot_village.wood.get(wood) & self.bot_village.stone.get(stone) & self.bot_village.iron.get(iron)
            logger.debug("Got treasury resources")
            yield self.bot_village.buildings.storage.queue.release(request)
            logger.debug("Treasury released")
        except simpy.Interrupt:
            logger.debug("Treasury lock was interrupted")


    def treasury(self, item, prio=0):
        request = self.bot_village.buildings.storage.queue.request(prio)
        request.item = item

        try:
            yield request
            logger.debug("Next cost: %d w %d s %d i %s" % (item.wood_cost, item.stone_cost, item.iron_cost, date(self.context.env.now)))
            yield self.bot_village.buildings.storage.wood_store.get(item.wood_cost) &\
                  self.bot_village.buildings.storage.stone_store.get(item.stone_cost) &\
                  self.bot_village.buildings.storage.iron_store.get(item.iron_cost)
            yield self.bot_village.treasurer.release(request)
            #print "Got treasury resources"
        except simpy.Interrupt:
            logger.debug("Treasury lock was interrupted")