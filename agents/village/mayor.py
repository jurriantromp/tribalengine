# Civil commander of a village

#todo broken (worldconfig)

from operator import itemgetter
from agents import Agent
import models.worldconfig
import models


def mayor(bot):
    #deel1 = self.building.cost / (self.building.production(True) - self.building.production(False)) # per uur
    #deel2 = self.unit.cost() / (self.village.get_max_unit_gain() * self.unit.amount * chance * (3600 / (self.unit.speed_base / (speed * unit_speed) * 120 * average_distance)))
    while True:
        building = min((bot.storage.wood_store.level, bot.wood), (bot.storage.stone_store.level, bot.stone),
                       (bot.storage.iron_store.level, bot.iron), key=itemgetter(0))[1]
        building.cost / (building.production(True) - building.production(False)) # is de periode groot genoeg?

        village = yield bot.local_villages.get(lambda village: village.get_weighted_gain() == 1.0)

        bot.spear.amount = 25 #todo zelf in kleine stapjes choppen
        bot.spear.cost() / (village.get_weighted_gain() * bot.spear.amount * chance * (
        bot.spear.speed_base / (models.worldconfig.speed * models.worldconfig.unit_speed) * 2 * village.distance))


class Mayor(Agent):
    def __init__(self, context, village):
        self.village = village
        super(Mayor, self).__init__(context)

    def upgrade_production(self):
        return self.interface.upgrade_production(self.village)

    def train_unit(self):
        return self.interface.train_unit(self.village)

    def attack_target(self):
        return self.interface.village_attack_target(self.village)
