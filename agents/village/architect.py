import variables

from builder import build
from config import HIGH_PRIO, LOW_PRIO
from misc.logger import logger


def architect(bot):
    def upgrade_barracks(bot):
        wait_time = sum(units.actual_build_time(bot.barracks.level) *
                        units.amount for units in bot.barracks_queue.GetQueue())
        if wait_time > 36000:  # 10 uur
            yield variables.env.process(build(bot.barracks))
        elif wait_time > 1:
            logger.debug("Current barracks wait time is", wait_time)

    def upgrade_storage(bot):
        percent = max(bot.storage.wood_store.level / bot.storage.wood_store.capacity * 100,
                      bot.storage.stone_store.level / bot.storage.stone_store.capacity * 100,
                      bot.storage.iron_store.level / bot.storage.iron_store.capacity * 100)
        if percent > 90:
            logger.warn("Storage is above 90% high prio upgrade")
            yield variables.env.process(build(bot.storage, HIGH_PRIO))
        elif percent > 70:
            logger.warn("Storage is between 70-90% low prio upgrade")
            yield variables.env.process(build(bot.storage, LOW_PRIO))

    def upgrade_farm(bot):
        percent = bot.farm.capacity() / bot.get_actual_population()
        if percent > 90:
            logger.warn("Farm is above 90% high prio upgrade")
            yield variables.env.process(build(bot.farm, HIGH_PRIO))
        elif percent > 70:
            logger.warn("Farm is between 70-90% low prio upgrade")
            yield variables.env.process(build(bot.farm, LOW_PRIO))

    while True:
        yield variables.env.process(upgrade_storage(bot))
        yield variables.env.process(upgrade_farm(bot))
        yield variables.env.timeout(100)
