from lxml import etree
import datetime

from misc.logger import logger


def heartbeat(module, bot):
    while True:
        beat = module.session.post("heartbeat", 'g', 0)

        #taggen
        if beat.get('player')["incomings"] > 0:
            page = module.session.html({'village': bot.id, 'screen': 'incomings'}, 'g')

            parser = etree.HTMLParser()
            tree = etree.fromstring(page, parser)
            incoming_ids = tree.xpath("//div[@id='show_incoming_units']//span[@class='quickedit']/@data-id")
            for id in incoming_ids:
                page = module.session.html({'village': bot.id, 'id': id, 'type': 'other', 'screen': 'info_command'},
                                           'g')
                parser = etree.HTMLParser()
                tree = etree.fromstring(page, parser)
                origin = tree.xpath("number(//div[@id='content_value']/table/tr[3]//span/@data-id)")
                target = tree.xpath("number(//div[@id='content_value']/table/tr[5]//span/@data-id)")
                raw_time = tree.xpath("string(//div[@id='content_value']//span[@class='timer']/text())")

                parts = raw_time.split(':')
                origin_village = player_villages[int(origin)]
                target_village = bot # todo bot_villages[int(target)]

                seconds = datetime.timedelta(hours=int(parts[0]), minutes=int(parts[1]),
                                             seconds=int(parts[2])).total_seconds()
                #{unit: unit.actual_speed() * origin_village.get_distance(target_village.x, target_village.y) for unit in bot.get_units()}
                slowest_unit = min(bot.get_units(), key=lambda unit: abs((
                                                                         unit.actual_speed() * 60 * origin_village.get_distance(
                                                                             target_village.x,
                                                                             target_village.y)) - seconds))
                logger.warn("Aanval van", origin, "op", target, "in", seconds, "slowest unit", slowest_unit)

        yield module.env.timeout(1)