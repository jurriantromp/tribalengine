import math

import variables
from config import DRY
from misc.utils import date
from models.unit import Infantry, Cavalry, Siege
from misc.logger import logger


class Recruiter(object):
    def __init__(self, context, village):
        self.context = context
        self.village = village

    def recruit(self, unit, amount=None):
        yield self.context.env.process(unit.check_requirements(self.village))

        while amount is None or amount > 0:
            min_amount = int(
                math.floor(
                    min(
                        self.village.wood.level / unit.wood,
                        self.village.stone.level / unit.stone,
                        self.village.iron.level / unit.iron,
                        (
                                self.village.buildings.farm.capacity() - self.village.get_actual_population()
                        ) / unit.pop - 50  # 50 slack
                    )
                )
            )

            if not amount:
                amount = min_amount
            elif min_amount < 10 and min_amount != amount:
                min_amount = 10
            elif min_amount > amount:
                min_amount = amount

            if min_amount == 0:
                break

            dummy = type(unit)(self.context)
            dummy.amount = min_amount

            wood = dummy.wood_cost
            stone = dummy.stone_cost
            iron = dummy.iron_cost

            if isinstance(unit, Infantry):
                request = self.village.buildings.barracks.queue.request()
            elif isinstance(unit, Cavalry):
                request = self.village.buildings.stable.queue.request()
            elif isinstance(unit, Siege):
                request = self.village.buildings.garage.queue.request()
            else:
                raise Exception('Unexpected unit')
            yield request

            yield self.context.env.process(self.village.treasurer.treasury_get(wood, stone, iron))
            start = self.context.env.now
            # if unit == infantry
            # if not DRY:
            #    self.village.session.post("train_do_train", 'g', self.village.id, "barracks", unit, min_amount)
            logger.info("Now recruiting %d %s, waiting %ds" % (min_amount, unit, unit.actual_build_time(self.village.buildings.barracks.level) * min_amount))
            for i in range(min_amount):
                yield self.context.env.timeout(unit.actual_build_time(self.village.buildings.barracks.level))
                self.village.units.spear.amount += 1
            logger.info("Recruited %d %s (%ds)" % (min_amount, unit, self.context.env.now - start))
            self.village.buildings.barracks.queue.release(request)

            amount -= min_amount

    def recruit_units(self):
        while True:
            yield self.context.env.process(self.recruit(self.village.units.light, 100))
            yield self.context.env.timeout(10)
