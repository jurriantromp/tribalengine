# Military commander of a village

import re

import models.report
from misc.logger import logger
from agents import Agent


class Captain(Agent):
    def attack_target(self, origin_village, target_village, unit_group):
        return self.interface.village_attack_target(origin_village, target_village, unit_group)

    def __init__(self, context):
        #interface = context.interface
        #self.bot_village = bot_village
        super(Captain, self).__init__(context)

    def get_commands(self): #todo not used
        response = self.bot_village.bot_player.session.html({'village': self.bot_village.id, 'try': 'confirm', 'screen': 'place'})

        # todo nog niet compatible met dagen in de toekomst
        commands = re.findall(
            r'([\w]+) om ([\d]{2}):([\d]{2}):([\d]{2})[^>]+>([\d]{3})[^\d]+([\d]{1,2}):([\d]{2}):([\d]{2})', response)

        #day = 19
        results = list()
        for c in commands:
            #if c[0] == 'vandaag':
            #    pass
            #elif c[0] == 'morgen':
            #    day += 1
            #else:
            #    raise ResultError("Datum nog niet ondersteund")

            #print datetime.datetime(2014, 4, day, int(c[1]), int(c[2]), int(c[3]), int(c[4]))

            results.append(int(c[5]) * 3600 + int(c[6]) * 60 + int(c[7]))
        return results


    def command(self, village, mode="attack", spear=0, sword=0, archer=0, axe=0, spy=0, light=0, marcher=0, heavy=0, ram=0, catapult=0, knight=0, snob=0):
        x = village.x
        y = village.y
        self.command_coordinates(x, y, mode, spear, sword, archer, axe, spy, light, marcher, heavy, ram, catapult, knight, snob)

    def command_coordinates(self, x, y, mode="attack", spear=0, sword=0, archer=0, axe=0, spy=0, light=0, marcher=0, heavy=0, ram=0, catapult=0, knight=0, snob=0):
        # http://nl38.tribalwars.nl/game.php?screen=place&village=26289&target=
        # POST http://nl38.tribalwars.nl/game.php?village=26289&action=command&h=4f92&screen=place
        # returns 503 naar place

        # support:Ondersteunen
        data = {
            "a431e0b862c3c5de871c72": "72e40314a431e0",
            "template_id": "",
            "spear": spear,
            "sword": sword,
            "archer": archer,
            "axe": axe,
            "spy": spy,
            "light": light,
            "marcher": marcher,
            "heavy": heavy,
            "ram": ram,
            "catapult": catapult,
            "knight": knight,
            "snob": snob,
            "x": x,
            "y": y,
            "target_type": "coord",
            "input": "",
            "attack": "Aanvallen",
        }

        for k, v in data.items():
            if (k != "x" or k != "y") and v == 0:
                data.pop(k, None)

        response = self.bot_village.bot_player.session.html({'village': self.bot_village.id, 'try': 'confirm', 'screen': 'place'}, 'g', data)

        error = re.search(r'class="error[^>]+>([^<]+)', response).group(1).strip()
        if not error == "":
            return False  # of een exception?

        ch = re.search(r'ch" value="([^"]+)', response).group(1)
        action_id = re.search(r'action_id" value="([^"]+)', response).group(1)
        csrf = re.search(r"csrf_token = '([^']+)", response).group(1)

        data = {
            mode: "true",
            "ch": ch, # ch appears to be a hash based on units and village
            "x": x,
            "y": y,
            "action_id": action_id,
            "spear": spear,
            "sword": sword,
            "archer": archer,
            "axe": axe,
            "spy": spy,
            "light": light,
            "marcher": marcher,
            "heavy": heavy,
            "ram": ram,
            "catapult": catapult,
            "knight": knight,
            "snob": snob,
        }

        for k, v in data.items():
            if (k != "x" or k != "y") and v == 0:
                data.pop(k, None)

        response = self.bot_village.bot_player.session.html({'village': self.bot_village.id, 'action': 'command', 'h': csrf, 'screen': 'place'}, 'g', data)

        #print response
        if session.status_code == 200:
            logger.info("Commando %s tegen %s,%s gestart" % (mode, x, y)) #todo better check if command did succeed
            #timeout = get_commands(session, bot)[-1] * 2
        else:
            logger.critical("Commando NIET gestart")
            return False
        return True


def attack(module, bot, target_village, units):
    bot.light.amount = 100
    yield bot.light.available.get(bot.light.amount)
    start = module.env.now
    assert 1 == 2
    #command(module.session, bot, target_village, "attack", spear, sword, archer, axe, spy, light, marcher, heavy, ram, catapult, knight, snob)
    #yield module.env.timeout(bot.light.actual_speed() * 120 * village.distance)
    bot.light.available.put(bot.light.amount)


def captain(module, bot):
    while True:
        amount = bot.light.available.level / 100
        for i in range(amount):
            bot.light.amount = 100
            bot.light.available.get(bot.light.amount)
            target_village = bot.local_villages.pop(0)  #(lambda village: village.distance == 1.0)
            start = module.env.now
            assert 1 == 2
            #command(module.session, bot, target_village, "attack", spear, sword, archer, axe, spy, light, marcher, heavy, ram, catapult, knight, snob)

            def attack():
                #yield module.env.timeout(bot.light.actual_speed() * 120 * village.distance)
                report = models.report.Report()
                report.date = module.env.now
                village.reports.append(report)
                put_resources(bot, report.wood_loot, report.stone_loot, report.iron_loot)
                logger.info("Looted village %d,%d on %d in %d: %d %d %d" % (
                village.x, village.y, module.env.now, module.env.now - start, report.wood_loot, report.stone_loot,
                report.iron_loot))

            bot.light.available.put(bot.light.amount)
            bot.local_villages.append(village)
            #yield module.env.process(attack())
        yield module.env.timeout(10)